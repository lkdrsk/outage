<?php
session_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/config/config.php'); 
if (!isset($_SESSION['SEbot'])) {
    $bot_UA=array(
'APIs-Google (+https://developers.google.com/webmasters/APIs-Google.html)',
'Mediapartners-Google',
'Mozilla/5.0 (Linux; Android 5.0; SM-G920A) AppleWebKit (KHTML, like Gecko) Chrome Mobile Safari (compatible; AdsBot-Google-Mobile; +http://www.google.com/mobile/adsbot.html)',
'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1 (compatible; AdsBot-Google-Mobile; +http://www.google.com/mobile/adsbot.html)',
'AdsBot-Google (+http://www.google.com/adsbot.html)',
'Googlebot-Image/1.0',
'Googlebot-News',
'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)',
'Googlebot/2.1 (+http://www.google.com/bot.html)',
'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)',
'Mediapartners-Google/2.1; +http://www.google.com/bot.html',
'AdsBot-Google-Mobile-Apps',

'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)',
'Mozilla/5.0 (iPhone; CPU iPhone OS 8_1 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12B411 Safari/600.1.4 (compatible; YandexBot/3.0; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexAccessibilityBot/3.0; +http://yandex.com/bots)',
'Mozilla/5.0 (iPhone; CPU iPhone OS 8_1 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12B411 Safari/600.1.4 (compatible; YandexMobileBot/3.0; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexDirectDyn/1.0; +http://yandex.com/bots',
'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36 (compatible; YandexScreenshotBot/3.0; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexImages/3.0; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexVideo/3.0; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexVideoParser/1.0; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexMedia/3.0; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexBlogs/0.99; robot; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexFavicons/1.0; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexWebmaster/2.0; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexPagechecker/1.0; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexImageResizer/2.0; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexAdNet/1.0; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexDirect/3.0; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YaDirectFetcher/1.0; Dyatel; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexCalendar/1.0; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexSitelinks; Dyatel; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexMetrika/2.0; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexNews/4.0; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexCatalog/3.0; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexMarket/1.0; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexVertis/3.0; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexForDomain/1.0; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexBot/3.0; MirrorDetector; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexSpravBot/1.0; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexSearchShop/1.0; +http://yandex.com/bots)',
'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36 (compatible; YandexMedianaBot/1.0; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexOntoDB/1.0; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexOntoDBAPI/1.0; +http://yandex.com/bots)',
'Mozilla/5.0 (compatible; YandexVerticals/1.0; +http://yandex.com/bots)',

'Mozilla/5.0 (compatible; Mail.RU_Bot/Fast/2.0)',

'StackRambler/2.0 (MSIE incompatible)',
'StackRambler/2.0',

'Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)',
'Mozilla/5.0 (compatible; Yahoo! Slurp/3.0; http://help.yahoo.com/help/us/ysearch/slurp)',

'msnbot/1.1 (+http://search.msn.com/msnbot.htm)',
'msnbot-media/1.0 (+http://search.msn.com/msnbot.htm)',
'msnbot-media/1.1 (+http://search.msn.com/msnbot.htm)',
'msnbot-news (+http://search.msn.com/msnbot.htm)',

'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm',

'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)',
'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html',
'Mozilla/5.0 (Linux;u;Android 2.3.7;zh-cn;) AppleWebKit/533.1 (KHTMLlike Gecko) Version/4.0 Mobile Safari/533.1 (compatible; +http://www.baidu.com/search/spider.html)',
'Baiduspider+(+http://www.baidu.com/search/spider.htm)',
'Mozilla/5.0 (Linux;u;Android 4.2.2;zh-cn;) AppleWebKit/534.46 (KHTML,like Gecko) Version/5.1 Mobile Safari/10600.6.3 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)',
'Baiduspider-image+(+http://www.baidu.com/search/spider.htm)',
'Mozilla/5.0 (compatible; heritrix/3.1.1 +http://www.baidu.com)',
'Mozilla/5.0 (compatible; Baiduspider-cpro; +http://www.baidu.com/search/spider.html)',
'Mozilla/5.0 (Linux;u;Android 2.3.7;zh-cn;) AppleWebKit/533.1 (KHTML,like Gecko) Version/4.0 Mobile Safari/533.1 (compatible; +http://www.baidu.com/search/spi_der.html)',
'User-Agent:Mozilla/5.0 (Linux;u;Android 2.3.7;zh-cn;) AppleWebKit/533.1 (KHTMLlike Gecko) Version/4.0 Mobile Safari/533.1 (compatible; +http://www.baidu.com/search/spider.html)',
'Mozilla/5.0 (Linux;u;Android 2.3.7;zh-cn;HTC Desire Build) AppleWebKit/533.1 (KHTMLlike Gecko) Version/4.0 Mobile Safari/533.1 (compatible; +http://www.baidu.com/search/spider.html)',
    );
    $bot_substring=array(
    '+http://www.google.com/bot.html',
    '+http://www.google.com/adsbot.html',
    '+http://yandex.com/bots',
    'http://help.yahoo.com/help/us/ysearch/slurp',
    '+http://search.msn.com/msnbot.htm',
    '+http://www.baidu.com/search/spider.htm',
    );
    if (in_array($_SERVER['HTTP_USER_AGENT'], $bot_UA) ) {
        //Bot detected!
        $_SESSION['SEbot']=$_SERVER['HTTP_USER_AGENT'];
    } else {
        foreach($bot_substring as $bot) {            
            if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), $bot)) {
                //Bot detected!
                $_SESSION['SEbot']=$_SERVER['HTTP_USER_AGENT'];
                break;                
            }
        }
        
    }
}

if (isset($_COOKIE['mycity']) && isset($_COOKIE['mydistrict']) && isset($_COOKIE['myregion'])) {
    if (preg_match(GUID_REGEX, $_COOKIE['myregion'], $matches)) {
        $myregion=$matches[0];
    }
    if (preg_match(GUID_REGEX, $_COOKIE['mydistrict'], $matches)) {
        $mydistrict=$matches[0];
    }
    if (preg_match(GUID_REGEX, $_COOKIE['mycity'], $matches)) {
        $mycity=$matches[0];
    }
} else {
    if (
        match_ip(ip2long($_SERVER['REMOTE_ADDR']),ip2long('10.0.0.0'), 8) ||
        match_ip(ip2long($_SERVER['REMOTE_ADDR']),ip2long('172.16.0.0'), 12) ||
        match_ip(ip2long($_SERVER['REMOTE_ADDR']),ip2long('192.168.0.0'), 16) 
       ) {
           $_SESSION['dadata']='This is LAN IP';
       }

    if (!isset($_SESSION['SEbot']) && !isset($_SESSION['dadata'])) {  //если это не бот и мы еще не знаем его GeoIP
        $curl = curl_init();
        if ($curl) {
            $header = array();
            $header[] = 'Content-type: application/json';
            $header[] = 'Accept: application/json';
            $header[] = 'Authorization: Token '.DADATA_APIKEY;
              
            $options = [
                    CURLOPT_URL => 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/iplocate/address?ip='.$_SERVER['REMOTE_ADDR'],
                    CURLOPT_RETURNTRANSFER => true, //всё, что вернётся из вызова, должно попасть в $result
                    CURLOPT_CONNECTTIMEOUT => 10,// таймаут
                    CURLOPT_SSL_VERIFYPEER => false, // не проверять сертификат
                    CURLOPT_SSL_VERIFYHOST =>0, // игнорировать, на какой хост идём
                    CURLOPT_HTTPHEADER => $header //заголовки
                ]; 
            curl_setopt_array($curl, $options);
            //$req_start=microtime(true);
            $_SESSION['dadata'] = json_decode(curl_exec($curl), true); // исполнение запроса
            
        } // $curl
    } // !isset    
}
//  $_SESSION['geo'] = json_decode(file_get_contents('http://ipwhois.app/json/'.$ip.'?lang=ru'), true);
//  $_SESSION['geo'] = json_decode(file_get_contents('http://api.sypexgeo.net/json/'.$ip), true);
    
$now_date= date("Y-m-d H:i:s");
include_once(SRV_DOC_ROOT.'/config/db.php'); //mysql DB
include(SRV_DOC_ROOT.'/inc/head.php'); 

$mod="user.php";
include(SRV_DOC_ROOT.'/mod/'.$mod);


include(SRV_DOC_ROOT.'/inc/end.php');
