<!--<!doctype html>-->
<?php header('Content-Type: text/html; charset=utf-8'); ?>
<!DOCTYPE html>
<html lang="ru" xml:lang="ru" xmlns="http://www.w3.org/1999/xhtml">
<head lang="ru">
<title>Портал отключений электроэнергии АО «ДРСК»</title>
<meta name="Description" content="Портал отключений электроэнергии АО «ДРСК»"> 
<meta name="Keywords" content="отключения, ДРСК, электроэнергия"> 
<meta charset="utf-8">
<meta name="msapplication-config" content="none"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<META name="apple-mobile-web-app-capable" content="yes">

<!--<link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" integrity="sha512-aOG0c6nPNzGk+5zjwyJaoRUgCdOrfSDhmMID2u4+OIslr0GjpLKo7Xm0Ao3xmpM4T8AmIouRkqwj1nrdVsLKEQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />

<!--<link href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet">-->


<!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.24/datatables.min.css"/>-->
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css"/>


<!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.24/datatables.min.css"/>-->


<link href="<?php echo $static_prefix.get_asset('/css/style.css', '/asset/style.min.css'); ?>" rel="stylesheet">
<meta name="viewport" content="width=device-width">
<META name="apple-mobile-web-app-capable" content="yes">

<?php
$user_agent=((isset($_SERVER['HTTP_USER_AGENT']) && !empty($_SERVER['HTTP_USER_AGENT']))?$_SERVER['HTTP_USER_AGENT']:'');
if(stristr ($user_agent, 'Windows Phone')){
?>
<!--Если мы на мобильном устройстве с windows-8-->
<style>
   @-ms-viewport{width:auto!important;}
</style>
<?php
}
?>
<!--эксперимент css  для третьего бутстрапа для дататаймпикера-->
<!--<link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" rel="stylesheet"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.min.css" rel="stylesheet"/>-->

<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->


<!--эксперимент-->


<!--<link href="/bootstrap/css/bootstrap-grid.css" rel="stylesheet"> 
<link href="/bootstrap/css/bootstrap-reboot.css" rel="stylesheet"> -->

<!--<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>-->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" integrity="sha512-aEe/ZxePawj0+G2R+AaIxgrQuKT68I28qh+wgLrcAJOz3rxCP+TwrK5SPN+E5I+1IQjNtcfvb96HDagwrKRdBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
 

<style type="text/css">
        .datepicker  {
            font-size: 0.875em;
        }
        .datetimepicker {
            font-size: 0.875em;

        }
    </style>


<!--<link type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.10.2/themes/overcast/jquery-ui.min.css" rel="stylesheet" />
<link type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.10.2/themes/overcast/jquery.ui.theme.min.css" rel="stylesheet" />-->
<style type="text/css">
.ui-widget {font-size:0.9em;

}

</style>
<!--<a href="http://drsk.ru"><img width="150" height="73" alt="Логотип компании" src="//res.cloudinary.com/drsk/raw/upload/q_auto,f_auto/lk/img/logo_color_gor_s.png"></a>-->
</head>
<body>

