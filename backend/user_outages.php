<?php
header("Content-Type: text/html; charset=utf-8");
$ret=array('html'=>'');
include_once($_SERVER['DOCUMENT_ROOT'].'/config/config.php');

$curl = curl_init();
if ($curl === false) {
    //...
    return false;
}
///////////////////////////////////////////////////////////обработка пользовательского ввода//////////////////////////////////
$err=0;
if( isset ($_POST)) {
    
//////////////////////////////////////////проверка e-mail/////////////////////////////////////////////////////
//$_SESSION['e-mail']=trim($_POST['mail']);
if(isset($_POST['tip_outages']) && ""==$_POST['tip_outages']) {

    switch ($_POST['feedback']) {
        case 1:
if (strlen(trim($_POST['mail']))==0) {
           $ret['err']['e-mail']="Введите e-mail";
           //$ret['err']=1;
           $err=1; 

 }
 elseif(!preg_match("|^[-0-9a-z_\.]+@[-0-9a-z_^\.]+\.[a-z]{2,6}$|i", trim($_POST['mail']))){ 
           $ret['err']['e-mail']="Вы ввели некорректный адрес электронной почты";
           //$ret['err']=1;
           $err=1;

 }
        break;
        case 2:
if (strlen(trim($_POST['phone']))==0) {
           $ret['err']['tel']="Введите номер телефона";
           //$ret['err']=1;
           $err=1; 

 }
    }
//////////////////////////////////////////проверка телефона///////////////////////////////////////////////////// 
 //$_SESSION['tel']=trim($_POST['phone']);

    
//////////////////////////////////////////проверка ФИО///////////////////////////////////////////////////// 
  //$_SESSION['fio']=trim($_POST['FIO']);
if ((intval($_POST['feedback'])!=3) && (strlen(trim($_POST['FIO']))==0)) {
           $ret['err']['fio_emptiness']="Введите ФИО";
           //$ret['err']=1;
           $err=1; 

}
 
 if (preg_match( '/[^а-яЁёА-Яa-zA-Z-\s]/u', trim($_POST['FIO'])) ){
    $ret['err']['fio_format']="ФИО может содержать только кириллические или латинские буквы, пробел и дефис";
    //$ret['err']=1;
    $err=1;
          
}
}

}
//echo($err."--> ошибка");
///////////////////////////////////////////////////////////обработка пользовательского ввода всё//////////////////////////////////
 

//$ret['html'].='<pre>'.var_export($_POST, true).'</pre>';  //вывести пост на экран
//$ret['html'].='<pre>ERR '.var_export($err, true).'</pre>';  //вывести ошибки на экран
//$ret['html'].='<pre>'.var_export($_SESSION,true).'</pre>';



//echo($tip_outages); 

$verbose = fopen('php://temp', 'w+');

//$ret['html'].='<pre> post:<BR>'.var_export($_POST['tip_outages'], true).'</pre>';  //вывести тип подключения на экран
//$ret['html'].='<pre> err:<BR>'.var_export($ret['err'], true).'</pre>';  //вывести ошибку на экран


if(isset($_POST['tip_outages']) && ""==$_POST['tip_outages']){   
   if (1!=$err){ //если не было ошибок пользовательского ввода, дёргаем сервис передачи адреса отключения
   //if ($_POST['user_error']){ //если не было ошибок пользовательского ввода, дёргаем сервис передачи адреса отключения

        $CURLOPT_URL='https://192.168.100.241:2829/et/hs/power_list'; //вебсервис (для передачи адреса отключения)  
        $req=json_encode(array(
//        'Period_start_date'=>date('YmdHis', strtotime($_POST['dateStart'])),
//        'Period_end_date'=>date('YmdHis', strtotime($_POST['dateFinish'].' 23:59:59')),
        'aRegion'=>$_POST['region_txt'],
        'aRayon'=>$_POST['district_txt'],
        'aCity'=>$_POST['city_txt'],
        //'Улица'=>$_GET['street_txt'],  // для выбора улицы по селекту
        'aStreet'=>$_POST['street_user'],  // для выбора улицы по инпуту
        'aNhouse'=>$_POST['house'],  //дом
        'aNcorp'=>$_POST['housing'],//корпус
        'aFIAS'=>$_POST['FIAS'],  // код ФИАС 
        //'vType'=>$_POST['tip_outages'],  // тип отключения
        ),  0); 
    }  // если не было ошибок пользовательского ввода, дёргаем сервис передачи адреса отключения  
    
}

$header = array();
$header[] = 'Content-type: application/json';
$header[] = 'Host: webpkupe.drsk.ru';
$header[] = 'Authorization: Basic '.PKUPE_AUTH;

    $options = [
            //CURLOPT_URL => 'https://192.168.100.241:2829/et/hs/Power_outage_log',  //первый веб-сервис (для выгрузки плановых и аварийных отключений)
            //CURLOPT_URL => 'https://192.168.100.241:2829/et/hs/power_outage_log_site', //второй вебсервис (для выгрузки плановых и аварийных отключений)
            CURLOPT_URL => $CURLOPT_URL,
            CURLOPT_POSTFIELDS => $req,
            CURLOPT_POST => true, // отдаём постом
            CURLOPT_RETURNTRANSFER => true, //всё, что вернётся из вызова, должно попасть в $result
            CURLOPT_CONNECTTIMEOUT => 10,// таймаут
          CURLOPT_VERBOSE => 1,// видеть дебаг
            CURLOPT_SSL_VERIFYPEER => false, // не проверять сертификат
            CURLOPT_SSL_VERIFYHOST =>0, // игнорировать, на какой хост идём
          CURLOPT_STDERR=>$verbose, // куда выводить дебаг
            CURLOPT_HTTPHEADER => $header //заголовки
        ]; 
    curl_setopt_array($curl, $options);
    $req_start=microtime(true);
    $result = json_decode(curl_exec($curl)); // исполнение запроса

    //$ret['html'].='Выполнено за '.(microtime(true)-$req_start).' сек';
       
if ($result === FALSE) {
    printf("cUrl error (#%d): %s<br>\n", curl_errno($handle),
           htmlspecialchars(curl_error($handle)));
}

rewind($verbose);
$verboseLog = stream_get_contents($verbose);


//$ret['html'].='Verbose information:\n<pre>'.htmlspecialchars($verboseLog).'</pre>'; // вывод логов на экран
//echo "Verbose information:\n<pre>", htmlspecialchars($verboseLog), "</pre>\n";

// var_dump($result->{'data'}[0]);
//$ret['html'].='RET: \n<pre>'.var_export($result, true).'</pre>';
//$ret['json']=array();
 
//        if (array()==$result->{'data'}) {

//        }
//        else {
//           foreach($result->{'data'} as $row) {
//                $ret['json'][]=array($row->{'rRegion_Rayon'}, $row->{'rCity'}, $row->{'rStreet_nDom'}, $row->{'DataSj'}, $row->{'rТехт_Date'} , $row->{'rFilial'}, $row->{'rСomment'});
//           }
//       }

// если  сервис передачи адреса отключения вернул не пустой идентификатор И у нас пустой массив с данными по плановыми и аварийным отключениям
        if (isset($result) ) {
            ////// вывод логов на экран////////////////
            
//                $ret['html'].='RET: \n<pre>попали в цикл, в котором дёргаем сервис по передаче отключений</pre>';
//                $ret['html'].='RET: \n<pre>'.var_export($result, true).'</pre>'; 
//                $ret['ID']=$result->{'ID'}; 
//                $ret['Result']=$result->{'Result'};
//                $ret['html'].='RET: \n<pre>'.$result->{'ID'}.'</pre>';    
//                $ret['html'].='RET: \n<pre>'.$result->{'Result'}.'</pre>';

            ////// вывод логов на экран всё////////////////   
            
            if('00000000-0000-0000-0000-000000000000'!=$result['ID']) {
                
            //////////////////////////ОПРЕДЕЛИМ IP ПОЛЬЗОВАТЕЛЯ//////////////////////////    
                        function getIp() {
                          $keys = [
                            'HTTP_CLIENT_IP',
                            'HTTP_X_FORWARDED_FOR',
                            'REMOTE_ADDR'
                          ];
                          foreach ($keys as $key) {
                            if (!empty($_SERVER[$key])) {
                              $ip = trim(end(explode(',', $_SERVER[$key])));
                              if (filter_var($ip, FILTER_VALIDATE_IP)) {
                                return $ip;
                              }
                            }
                          }
                        }

                        $ip = getIp();
                                        
              //////////////////////////ОПРЕДЕЛИМ IP ПОЛЬЗОВАТЕЛЯ ВСЁ//////////////////////////
              
                
                //$ret['html'].='RET: \n<pre>дёргаем сервис https://192.168.100.241:2829/et/hs/power_log_site</pre>';
                //$ip=$_SERVER["REMOTE_ADDR"];
                //$ret['html'].='RET: \n<pre>IP: '.$ip.'</pre>';
                $CURLOPT_URL='https://192.168.100.241:2829/et/hs/power_log_site'; //вебсервис (для передачи ID (идентификатора) отключения);        
                $req=json_encode(array(
                'ID'=>$ret['ID'],
                //'DataR'=>$_POST['outage_user_date'].' '.$_POST['outage_user_time'].':00',   //это верный формат, эту строку открыть, а строку ниже-закомментировать
                'DataR'=>$_POST['outage_user_date'].' '.$_POST['outage_user_time'],
                'FIO'=>$_POST['FIO'],
                'Ntelefon_Email'=>$_POST['mail'].' '.$_POST['phone'],
                'pFault'=>$_POST['danger'], //есть ли повреждения      
                'eTime'=>$_POST['duration'],// продолжительность отключения  
                'eIP'=>$ip,  //ip юзера
                'eSave'=>'нет',
                 ),  0); 
                 
                 
    $options = [
            CURLOPT_URL => $CURLOPT_URL,
            CURLOPT_POSTFIELDS => $req,
            CURLOPT_POST => true, // отдаём постом
            CURLOPT_RETURNTRANSFER => true, //всё, что вернётся из вызова, должно попасть в $result_last
            CURLOPT_CONNECTTIMEOUT => 10,// таймаут
            CURLOPT_VERBOSE => 1,// видеть дебаг
            CURLOPT_SSL_VERIFYPEER => false, // не проверять сертификат
            CURLOPT_SSL_VERIFYHOST =>0, // игнорировать, на какой хост идём
            CURLOPT_STDERR=>$verbose, // куда выводить дебаг
            CURLOPT_HTTPHEADER => $header //заголовки
        ]; 
        curl_setopt_array($curl, $options);

           $req_start=microtime(true);
//$ret['html'].='REQ: \n<pre>'.var_export($req, true).'</pre>';

           $result_last = json_decode(curl_exec($curl)); // исполнение запроса

          // $ret['html'].='Выполнено за '.(microtime(true)-$req_start).' секУНД';
       
        if ($result_last === FALSE) {
            printf("cUrl error (#%d): %s<br>\n", curl_errno($handle),
                   htmlspecialchars(curl_error($handle)));
        }

        rewind($verbose);
        $verboseLog = stream_get_contents($verbose);
                 
            } //if('00000000-0000-0000-0000-000000000000'!=$result['ID'])
            
     
     //$ret['html'].='Verbose information:\n<pre>'.htmlspecialchars($verboseLog).'</pre>';
        //Получаем конечный ответ от сервиса Power_log_site///////////////////
            if (isset($result_last)){
                $ret['html'].='RET: \n<pre>'.var_export($result_last, true).'</pre>';                                  
            }
//            else{
        //         $ret['html'].='RET: \n<pre>пусто</pre>';
//            }
     
        //Получили конечный ответ от сервиса Power_log_site///////////////////
        } //(isset($result) && array()==$result->{'data'})
// если  сервис передачи адреса отключения вернул не пустой идентификатор всё
        

echo json_encode($ret);
