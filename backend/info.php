<?php
header('Content-Type: application/json; charset=utf-8');    #RFC4627 https://www.ietf.org/rfc/rfc4627.txt
include_once($_SERVER['DOCUMENT_ROOT'].'/config/config.php');
require(SRV_DOC_ROOT.'/config/db.php'); 
$result=array();

if (isset($_POST['term']) && isset($_POST['level_4_6'])) {
    $sql='SELECT  OFFNAME, AOGUID, SHORTNAME,  FORMALNAME, PARENTGUID FROM AddressObject  WHERE parentguid=\''.trim($_POST['level_4_6']).'\' and actstatus=1 and livestatus=1 and aolevel in (7) and (lower(OFFNAME) LIKE lower(\'%'.trim($_POST['term']).'%\')) order by offname ASC, aolevel ASC';
    $res=pg_query($fias_link, $sql);
    if ($res) {
        while ($arr=pg_fetch_array($res)) {  
            //RTFM: https://api.jqueryui.com/autocomplete/#option-source
            //                         label => как показываем вариант юзеру            value => что будет помещено в input при выборе этого варианта
            array_push($result, array('label'=>$arr['shortname'].' '.$arr['offname'],  'value'=>$arr['offname'],                                       'fias' => $arr['aoguid'] )); 
        }
    }
}
echo json_encode($result);