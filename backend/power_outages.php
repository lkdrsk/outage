<?php
header("Content-Type: text/html; charset=utf-8");
//$ret=array('html'=>'');
$ret=array();
include_once($_SERVER['DOCUMENT_ROOT'].'/config/config.php');

$curl = curl_init();
if ($curl === false) {
    return false;
}
 
//$ret['html'].='<pre>'.var_export($_POST, true).'</pre>';  //вывести пост на экран
//$ret['html'].='<pre>'.var_export($_SESSION,true).'</pre>';

$verbose = fopen('php://temp', 'w+');

if (isset($_POST['tip_outages'])) {
    $log_filename='log/power_outage-'.$_SERVER['REMOTE_ADDR'].'.log';
    @file_put_contents($log_filename,date(DATE_ATOM)." request:\n".var_export($_POST, true)."\n", FILE_APPEND);    
    
    switch (intval($_POST['tip_outages'])) {
        case 1:
            $vtype="П";
            break;
        case 2:
            $vtype="АВ";
            break;
        default:
            $vtype='';
    }

//Запомнить в куке город, район, регион на 10 минут
setcookie("mycity",     $_POST['city_fias'],     time()+60*60*24, '/', NULL, false, true);
setcookie("mydistrict", $_POST['district_fias'], time()+60*60*24, '/', NULL, false, true);
setcookie("myregion",   $_POST['region_fias'],   time()+60*60*24, '/', NULL, false, true);

        $CURLOPT_URL='https://192.168.100.241:2829/et/hs/power_outage_log_site'; //второй вебсервис (для выгрузки плановых и аварийных отключений) 
        $req=json_encode(array(
        'Period_start_date'=>date('YmdHis', strtotime($_POST['dateStart'])),
        'Period_end_date'=>date('YmdHis', strtotime($_POST['dateFinish'].' 23:59:59')),
        'aRegion'=>$_POST['region_txt'],
        'aRayon'=>$_POST['district_txt'],
        'aCity'=>$_POST['city_txt'],
        'aStreet'=>$_POST['street_user'],
        'aFIAS'=>$_POST['FIAS'],  // код ФИАС 
        'vType'=>$vtype,
        ),  0);
        
} //if(isset($_POST['tip_outages']) && ""!=$_POST['tip_outages'])

$header = array();
$header[] = 'Content-type: application/json';
$header[] = 'Host: webpkupe.drsk.ru';
$header[] = 'Authorization: Basic '.PKUPE_AUTH;

$options = [
            //CURLOPT_URL => 'https://192.168.100.241:2829/et/hs/Power_outage_log',  //первый веб-сервис (для выгрузки плановых и аварийных отключений)
            //CURLOPT_URL => 'https://192.168.100.241:2829/et/hs/power_outage_log_site', //второй вебсервис (для выгрузки плановых и аварийных отключений)
            CURLOPT_URL => $CURLOPT_URL,
            CURLOPT_POSTFIELDS => $req,
            CURLOPT_POST => true, // отдаём постом
            CURLOPT_RETURNTRANSFER => true, //всё, что вернётся из вызова, должно попасть в $result
            CURLOPT_CONNECTTIMEOUT => 10,// таймаут
            CURLOPT_VERBOSE => 1,// видеть дебаг
            CURLOPT_SSL_VERIFYPEER => false, // не проверять сертификат
            CURLOPT_SSL_VERIFYHOST =>0, // игнорировать, на какой хост идём
            CURLOPT_STDERR=>$verbose, // куда выводить дебаг
            CURLOPT_HTTPHEADER => $header //заголовки
        ]; 
curl_setopt_array($curl, $options);
$req_start=microtime(true);
$result = json_decode(curl_exec($curl)); // исполнение запроса
@file_put_contents($log_filename,'Result ('.(microtime(true)-$req_start).'s):'."\n".var_export($result, true)."\n\n", FILE_APPEND);    
       
//if ($result === FALSE) {
//    printf("cUrl error (#%d): %s<br>\n", curl_errno($handle),
//           htmlspecialchars(curl_error($handle)));
//}

rewind($verbose);
$verboseLog = stream_get_contents($verbose);

//$ret['html'].='Verbose information:\n<pre>'.htmlspecialchars($verboseLog).'</pre>'; // вывод логов на экран
//echo "Verbose information:\n<pre>", htmlspecialchars($verboseLog), "</pre>\n";

//$ret['html'].='RET: \n<pre>'.var_export($result, true).'</pre>';
$ret['json']=array();

        
        //if (array()==$result->{'data'} && ""!=$tip_outages) { 
     //   if (array()!=$result->{'data'}) { //если есть данных об отключениях
     
     
    switch (intval($_POST['tip_outages'])) {
        case 1:
            foreach($result->{'data'} as $row) {
                        $ret['json'][]=array($row->{'rRegion_Rayon'}, $row->{'rCity'}, $row->{'rStreet_nDom'}, $row->{'DataSj'}, $row->{'rТехт_Date'}, $row->{'rFilial'}, $row->{'rСomment'});
            }
            break;
        case 2:
            foreach($result->{'data'} as $row) {
                        $ret['json'][]=array($row->{'rRegion_Rayon'}, $row->{'rCity'}, $row->{'rStreet_nDom'},                   $row->{'rТехт_Date'}, $row->{'rFilial'}                    );
            }               
            break;
        default:
            $vtype='';
    }
    //    }    //else --> если массив
echo json_encode($ret);