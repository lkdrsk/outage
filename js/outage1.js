 
//Small jQuery plugin to filter a child <select> based on a parent <select>
//from https://gist.github.com/MartinL83/9940319
jQuery.fn.filterSelectByParent = function(child) {
        return this.each(function() {
                var parent = this;
                var parentOptions = [];
                var childOptions = [];

                var childFirst = jQuery(child).find('option:first').attr('data-parent');
                var childFirstText = jQuery(child).find('option:first').text();
                var parentValue = jQuery(parent).val();
                var childValue = jQuery(child).val();
                
                //Push parent options value to parentOptions array
                jQuery(parent).find('option').each(function() {
                        var self = jQuery(this);
                        var selectedValue;
                        if(parentValue === self.val()) {
                                selectedValue = true;
                        }
                        else {
                                selectedValue = false;
                        }
                        parentOptions.push({
                                value: self.val(),
                                text: self.text(),
                                optval: self.attr('data-optval'),
                                selected: selectedValue
                        });
                });
                
                //Push child options value to childOptions array
                jQuery(child).find('option').each(function() {
                        var self = jQuery(this);
                        var selectedValue;
                        if(childValue === self.val()) {
                                selectedValue = true;
                        }
                        else {
                                selectedValue = false;
                        }
                        childOptions.push({
                                value: self.val(),
                                text: self.text(),
                                parent: self.attr('data-parent'),
                                optval: self.attr('data-optval'),
                                selected: selectedValue
                        });
                });

                //Store parent & child options from the previous arrays.
                jQuery(parent).data('options', parentOptions);
                jQuery(child).data('options', childOptions);

                //Function to filter child select
                var filter = function(){
                        var self = jQuery(this);
                        var selectedValue = jQuery(parent).val();
                        var options = jQuery(child).empty().data('options');
                        
                        //Loop trough each <option> in child.
                        jQuery.each(options,function(i){
                                var option = options[i];
                                if(selectedValue === option.parent) {
                                        //Create option(s);
                                        var filteredOption = jQuery('<option>').text(option.text).val(option.value).attr('data-optval', option.optval);
                                        
                                        //If option was selected, add selected tag.
                                        if(option.selected === true){
                                                filteredOption.attr('selected','selected');
                                        }
                                        //Append new option to child select.
                                        jQuery(child).append(filteredOption);
                                }
                        });
                        /*
                        if(childFirst === 'all') {
                                //Create <option> that shows all citys (val="0");
                                var allOption = jQuery('<option>').text(childFirstText).val('0');
                                
                                //Add <option> to child select.
                                jQuery(child).prepend(allOption);
                                
                                //If current selected value = 0.
                                if(childValue === '0') {
                                        allOption.attr('selected','selected');
                                }
                        }
                        */
                        jQuery(child).change();    
                        
                        
                };
                
                //On parent <select> change, do filter();
                jQuery(parent).change(function(){
                        filter();
                });
                
                //Init filter.
                filter();

        });
};

jQuery.extend( jQuery.fn.dataTableExt.oSort, {
"dateHM_ru-pre": function ( a ) {
    if (a) {
        var ruDateTimea = a.split(' ');    
        var ruDatea = ruDateTimea[0].split('.');
        var ruTimea = ruDateTimea[1].split(':');
        //console.log(   (ruDatea[2] + ruDatea[1] + ruDatea[0] + ruTimea[0] + ruTimea[1]) *1 );
        return (   (ruDatea[2] + ruDatea[1] + ruDatea[0] + ruTimea[0] + ruTimea[1]) *1 );
    } else {
        //console.log(a);
        return 0;
    }
},

"dateHM_ru-asc": function ( a, b ) {
    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
},

"dateHM_ru-desc": function ( a, b ) {
    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
}
} );



function show_container (num) {
container_view=num.toString();
tip_outages=num.toString();
switch (container_view) {     
    case '3': // сообщить об отключениях
        //tip_outages='';    
        $('#container_1').addClass('d-none');
        $('#container_2').addClass('d-none');
        $('#container_3').removeClass('d-none');
        $('.outage_1_2').addClass('d-none');
        $('.outage_3').removeClass('d-none');
        break;

    case '2': //аварийные
        //tip_outages='АВ';
        $('#container_1').addClass('d-none');
        $('#container_2').removeClass('d-none');        
        $('#container_3').addClass('d-none');
        $('.outage_1_2').removeClass('d-none');          
        $('.outage_3').addClass('d-none');
        break;          
        
    case '1': //плановые
    default:
        //tip_outages='П';
        $('#container_1').removeClass('d-none');
        $('#container_2').addClass('d-none');
        $('#container_3').addClass('d-none');
        $('.outage_1_2').removeClass('d-none');       
        $('.outage_3').addClass('d-none');
        break;
}
        
        $('.out_tip').addClass('d-none'); //все спрятять
        $('#out_tip_'+container_view).removeClass('d-none'); //а текущий показать
    
}

function liFormat (row, i, num) {      
  var expr = new RegExp('('+row[1]+')', 'i');
  return row[0].replace(expr,'<b>$1</b>');
} 

function selectItem(event, ui) {    
  $('#level_7').focus();
} 


var ac;
$(document).ready(function(){  
    $('.datepicker').datepicker({
        language: 'ru',
//        daysOfWeekHighlighted: "6,0",
        autoclose: true,
        todayHighlight: true,
        todayBtn: 'linked',
        orientation: 'bottom',        
    });
    

//    $(function () {
      $('#outage_user_date_time').datetimepicker(
        { locale: 'ru' }
      );
//    });
   
//       $(function () {
      $('#outage_user_time').datetimepicker(
        { locale: 'ru',
          format: 'HH:mm',
          allowInputToggle: true,
        }
      );
//    });
    
     
    
    $('#dt1').DataTable({
        aoColumns: [
            null,
            null,
            null,
            { "sType": "dateHM_ru" },
            { "sType": "dateHM_ru" },
            null,
            null
        ],
        pageLength: 50,
        language: {
           // url: 'https://cdn.datatables.net/plug-ins/1.10.24/i18n/Russian.json', 
            "processing": "Загрузка данных ",
              "search": "Поиск:",
              "lengthMenu": "Показать _MENU_ записей",
              "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
              "infoEmpty": "Записи с 0 по 0 из 0 записей",
              "infoFiltered": "(отфильтровано из _MAX_ записей)",
              "loadingRecords": "Загрузка данных",
              "zeroRecords": "Записи отсутствуют",
              "emptyTable": "По Вашему запросу информация не найдена",
              "paginate": {
                "first": "Первая",
                "previous": "Предыдущая",
                "next": "Следующая",
                "last": "Последняя"
              },
              "aria": {
                "sortAscending": ": активировать для сортировки столбца по возрастанию",
                "sortDescending": ": активировать для сортировки столбца по убыванию"
              },
              "select": {
                "rows": {
                  "_": "Выбрано записей: %d",
                  "0": "Кликните по записи для выбора",
                  "1": "Выбрана одна запись"
                }
              },            
            decimal: ',',
            thousands: '',     
        },
        });

    $('#dt2').DataTable({
        aoColumns: [
            null,
            null,
            null,
            { "sType": "dateHM_ru" },
            null
        ],
        pageLength: 50,
        language: {
           // url: 'https://cdn.datatables.net/plug-ins/1.10.24/i18n/Russian.json', 
            "processing": "Загрузка данных ",
              "search": "Поиск:",
              "lengthMenu": "Показать _MENU_ записей",
              "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
              "infoEmpty": "Записи с 0 по 0 из 0 записей",
              "infoFiltered": "(отфильтровано из _MAX_ записей)",
              "loadingRecords": "Загрузка данных",
              "zeroRecords": "Записи отсутствуют",
              "emptyTable": "По Вашему запросу информация не найдена",
              "paginate": {
                "first": "Первая",
                "previous": "Предыдущая",
                "next": "Следующая",
                "last": "Последняя"
              },
              "aria": {
                "sortAscending": ": активировать для сортировки столбца по возрастанию",
                "sortDescending": ": активировать для сортировки столбца по убыванию"
              },
              "select": {
                "rows": {
                  "_": "Выбрано записей: %d",
                  "0": "Кликните по записи для выбора",
                  "1": "Выбрана одна запись"
                }
              },            
            decimal: ',',
            thousands: '',     
        },
        });
        
   ac=$("#level_7").autocomplete( { 
   source: function( request , response ) {
        var param = { level_1: $('#level_1').val(), level_3_4: $('#level_3_4').val(), level_4_6: $('#level_4_6').val(), term: request.term } ;
        
        if (null!=$('#level_4_6').val()) {   //если выбран город
            
        $.ajax({
            url: "/backend/info.php",
            data : param,
            dataType: "json",
            type: "POST",
            success: function (data) {
            response(data);

            }            
        });
        
        }   //если выбран город
    } ,
    minLength: 3, delay:300, autoFocus: true, select: function( event, ui ) {
        $('#level_7').val(ui.item.value);
        $('#level_7').attr('data-fias', ui.item.fias);
        $('#level_7').attr('data-offname', ui.item.value);
        //$('#level_7').focus();
        //$('#address').submit();
            return true;       
    }
       
   }
    );

 
        $("#level_1, #level_3_4, #level_4_6").change(function() {
            $('#level_7').val('');           
        });

    
$('.outage_tip').click(function (){
    //container_view='';
    //user_error=0; // ошибка пользовательского ввода обнулена

//console.log('click: '+$(this).attr('data-id'));
//    console.log('current: '+tip_outages);    
//    if ($(this).attr('data-id')!=tip_outages) {
//        console.log('Need clear');        
//    }
    
    show_container($(this).attr('data-id'));
    
    return false;    
});

$('.get_outages').click(function () { 
    var _this = $(this);
    
    if (undefined==$('#level_1').find('option:selected').attr('data-region')) {
        $('#level_1').focus();
        return false;
    }

    var fias;
    if ($('#level_7').attr('data-offname')!=$('#level_7').val()) {
        $('#level_7').attr('data-offname', '');
        $('#level_7').attr('data-fias', '');
        fias='';
    } else {
        fias=$('#level_7').attr('data-fias');        
    }
    if (fias=='') {
        fias=$('#level_4_6').find('option:selected').val();        
    }

    var _data={};
    switch (_this.attr('id')) {
        case 'get_planned_outages':
            _url='/backend/power_outages.php';
            _data={
         'region': $('#level_1').find('option:selected').attr('data-region'),
         'region_txt':  $('#level_1').find('option:selected').attr('data-optval'),
         'region_fias': $('#level_1').find('option:selected').val(),

         'district_fias': $('#level_3_4').find('option:selected').val(),         
         'district_txt':  $('#level_3_4').find('option:selected').attr('data-optval'),

         'city_fias':  $('#level_4_6').find('option:selected').val(),         
         'city_txt':  $('#level_4_6').find('option:selected').attr('data-optval'),         
         
         'street': $('#level_7').find('option:selected').val(),
         'street_txt': $('#level_7').find('option:selected').attr('data-optval'),   
         'street_user': $('#level_7').val(),
         
         'dateStart': $('#outage_start').val(),
         'dateFinish': $('#outage_finish').val(),
         'tip_outages': tip_outages,
         'FIAS': fias         
        };
            break;
        default:
            _url='/backend/user_outages.php';
            _data={
         'region_txt':  $('#level_1').find('option:selected').attr('data-optval'),
         'district_txt':  $('#level_3_4').find('option:selected').attr('data-optval'),
         'city_txt':  $('#level_4_6').find('option:selected').attr('data-optval'),         
         'street_txt': $('#level_7').find('option:selected').attr('data-optval'),   
         
         'region': $('#level_1').find('option:selected').attr('data-region'),
         'district': $('#level_3_4').find('option:selected').val(),         
         'street': $('#level_7').find('option:selected').val(),
         
         'dateStart': $('#outage_start').val(),
         'dateFinish': $('#outage_finish').val(),
         'tip_outages': tip_outages,
         'street_user': $('#level_7').val(),
         'FIAS': fias,
         
         //для передачи информации об отключениях:////////////////////
         'time_outage': $('#outage_user_time').val(),//не используется
         'house': $('#user_house').val(),
         'housing': $('#housing').val(), 
         'outage_user_date': $('#outage_user_date').val(),
         'outage_user_time': $('#outage_user_time').val(),
         //вид связи с клиентом//    
         'feedback_type': $('.feedback:checked').val(),
         'mail': $('#communication-1').val(),
         'phone': $('#communication-2').val(),
         
         //повреждение э/оборудования, угроза поражения эл. током или возгорания//
         'danger': $('.danger:checked').val(),
         //продолжительность отключения//
         'duration': $('.duration:checked').val(),                 
         //фамилия, имя, отчество клиента
         'FIO': $('#fio').val(),
         //идентификатор вида связи с клиентом, не передаётся в сервис, нужен для обнуления ошибки пустоты ФИО при выборе вида связи "не требуется"
         'feedback': $('input[name="feedback_type"]:checked').attr('id'),
         //'all_rows': all_rows,
         //'user_error': user_error, // ошибка пользовательского ввода
         
        };
            
    }
    
    
    $.ajax({
        url: _url,
        type: "POST",
        timeout: 20000,
        data: _data,       
        dataType: 'json', 
        
        ///////////////////////////////////////////////////////////////////////////////////////  
        beforeSend: function () {
        // делам кнопку недоступной и отображаем спиннер 
         _this.prop('disabled',true);
         _this.addClass('d-none');
         $('#info-load').removeClass('d-none');         
         
         //console.log(_data);
      },
      
      //////////////////////////////////////////////////////////////////////////////////////          
        success: function (data, textStatus, jqXHR ) {               
            //obj.hasOwnProperty('prop') проверяет объект на наличие собственного свойства; help: https://tproger.ru/translations/how-to-handle-undefined-in-javascript/
            $('#communication-1_error').html('');
            $('#communication-2_error').html('');
            $('#fio_error_emptiness').html('');
            $('#fio_error_format').html('');
            
            
            if (data.hasOwnProperty('err')) {
                var err=data['err'];
                    
                if (err.hasOwnProperty('e-mail')) {  
                    $('#communication-1_error').html(err['e-mail']);
                }        
                
                //обработка ошибки телефона/////////
                if (err.hasOwnProperty('tel')) {
                    $('#communication-2_error').html(err['tel']);
                }        
            
                //обработка ошибки ФИО --если поле пустое/////////
                if (err.hasOwnProperty('fio_emptiness')) {
                    $('#fio_error_emptiness').html(err['fio_emptiness']);
                }
                
                //обработка ошибки ФИО --если введён неверный формат в поле/////////   
                if (err.hasOwnProperty('fio_format')) {     
                    $('#fio_error_format').html(err['fio_format']);               
                }
            }
            
           // $('.feedback').each(function() {
           //$('#container_' + container_view).html(data['html']);
           
           if (data.hasOwnProperty('Result')) {
               $('#span_for_ID').html(data['Result']);   
           }
           else{
               $('#span_for_ID').html('');
           }
           ///////////////////////////////////////вывод идентификатора Костиной записи всё//////////////////  
           
           ///////////////////////////////////////вывод поста в user//////////////////
           $('#span_for_post').html(data['html']);
           ///////////////////////////////////////вывод поста в user всё//////////////////  
                      
           
           // $('#container_' + container_view).html(data['html'].slice(0,70)); // выбрать несколько строк
        
           //очистка таблицы (по id) с данными об отключениях и её построчное заполнение  данными//////////////////////////
           var _table=$('#dt'+container_view).DataTable();
           _table.clear().rows.add(data['json']).draw() ;    
           all_rows=_table.data().length; //записей в таблице
            
           
           if ( all_rows>0) { // если в таблице есть записи, отобразим её
               $('#table_'+container_view).removeClass('d-none'); // показываем таблицу по id С ДАННЫМИ
               $('#noMatch_'+container_view).addClass('d-none');  //скрываем div по id с сообщением, что информация по запросу не найдена            
           }
            else{        
              $('#table_'+container_view).addClass('d-none');   // скрываем таблицу по id БЕЗ ДАННЫХ
              $('#noMatch_'+container_view).removeClass('d-none');  //показываем div по id с сообщением, что информация по запросу не найдена
            } 
           
        },
         
         
        complete: function (jqXHR, textStatus) {
            _this.removeClass('d-none');
            _this.prop("disabled",false);
            //_this.prop("disabled",false).find('.spinner-border').css('visibility', 'hidden'); //.addClass('d-none'); 
                                          
        }
      
     
    })   
     //////////////////////////////////////////////////////////////////////////////////////
      .always(function(){
        // скрываем кнопку спиннера
        $('#info-load').addClass('d-none');
         //_this.css('visibility', 'hidden');
        
      });
     ///////////////////////////////////////////////////////////////////////////////////////    
});
      
//////////////////////////////////////пользовательский ввод:обязательность полей и видимость полей телефона и e-mail, маска телефона///////////////////
$('input[name="feedback_type"]').click(function(){
    $('.feedback_view').addClass('d-none');
    $('.communication_type').removeAttr('required');    
    
//    $('#user_info-' + $(this).val()).removeClass('d-none');    
//    $('#communication-' + $(this).val()).attr('required','');  
    
    $('#user_info-' + $(this).attr('id')).removeClass('d-none');    
    $('#communication-' + $(this).attr('id')).attr('required','');
    
    //при смене типа связи очищаем ошибки
    $('#communication-1_error').html('');
    $('#communication-2_error').html('');
    
    ////////если обратная связь не требуется///////////////
    
    if(3==$(this).attr('id')){
        //alert($(this).attr('id'));
        $('.user-fio').removeAttr('required');
        $('.star_fio').addClass('d-none'); 
        $('#fio_error_emptiness').html('');  
               
    }
    else{
        $('.user-fio').attr('required','');
        $('.star_fio').removeClass('d-none');   
    }
    
});

jQuery(function($) {
      $.mask.definitions['~']='[+-]';
      $('#communication-2').mask('+7 (999) 999-9999');     
});
//////////////////////////////////////пользовательский ввод:обязательность полей и видимость полей телефона и e-mail всё/////////////////// 
      
 
if (typeof myregion !== 'undefined') {
    if ($('#level_1 option[value='+myregion+']').length>0) {
        $('#level_1').val(myregion);    
    }
}

if (typeof mydistrict !== 'undefined') {
    if ($('#level_3_4 option[value='+mydistrict+']').length>0) {
        $('#level_3_4').val(mydistrict);
    }
}

if (typeof mycity !== 'undefined') {
    if ($('#level_4_6 option[value='+mycity+']').length>0) {    
        $('#level_4_6').val(mycity);
    }
}

$('#level_4_6').filterSelectByParent('#level_7');
$('#level_3_4').filterSelectByParent('#level_4_6');
$('#level_1').filterSelectByParent('#level_3_4');

//сообщение после нажатия юзером кнопки "сообщить об отключениях"
$('#user_send_outages').click(function () {
    $('#alert_user').removeClass('d-none');
    window.setTimeout(function(){
            $('#alert_user').addClass('d-none');
        },5000);
    });
    

   
    $('#loader_fias').addClass('d-none');
        container_view='';
        data_table_view_1='';
        data_table_view_2='';
        all_rows=0;
        
        
        show_container(tip_outages);
});


function formatDatepicker(elem_id) {
                var out_str = $('#'+elem_id).val().replace(new RegExp('\\D+', 'g'), '');
                var p1 = out_str.substr(0, 2);
                var p2 = out_str.substr(2, 2);
                var p3 = out_str.substr(4, 4);
                 if (p3.length > 0) {
                    out_str = p1 + '.' + p2 + '.' + p3;
                } else if (p2.length > 0) {
                    out_str = p1 + '.' + p2;
                } else if (p1.length > 0) {
                    out_str = p1;
                } else {
                    out_str = ''; 
                }
                $('#'+elem_id).val(out_str);               
            }

          
            
function formatDatetimepicker(elem_id) {
    var out_str = $('#'+elem_id).val();
                  $('#'+elem_id).val(out_str);               
            } 



